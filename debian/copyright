Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/danni/python-pkcs11/
Upstream-Name: python-pkcs11
Upstream-Contact: Danielle Madeley <danielle@madeley.id.au>

Files: *
Copyright:
 2017 Danielle Madeley
License: Expat

Files:
 extern/cryptoki.h
 pkcs11/_mswin.pxd
Copyright:
 2019 Eric Devolder
License: Expat

Files:
 extern/pkcs11.h
 extern/pkcs11t.h
 extern/pkcs11f.h
Copyright:
 2016 OASIS Open
License: OASIS

Files: debian/*
Copyright:
 2023 Faidon Liambotis <paravoid@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: OASIS
 All capitalized terms in the following text have the meanings assigned to them
 in the OASIS Intellectual Property Rights Policy (the "OASIS IPR Policy"). The
 full Policy may be found at the OASIS website.
 .
 This document and translations of it may be copied and furnished to others, and
 derivative works that comment on or otherwise explain it or assist in its
 implementation may be prepared, copied, published, and distributed, in whole or
 in part, without restriction of any kind, provided that the above copyright
 notice and this section are included on all such copies and derivative works.
 However, this document itself may not be modified in any way, including by
 removing the copyright notice or references to OASIS, except as needed for the
 purpose of developing any document or deliverable produced by an OASIS
 Technical Committee (in which case the rules applicable to copyrights, as set
 forth in the OASIS IPR Policy, must be followed) or as required to translate it
 into languages other than English.
 .
 The limited permissions granted above are perpetual and will not be revoked by
 OASIS or its successors or assigns.
 .
 This document and the information contained herein is provided on an "AS IS"
 basis and OASIS DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
 INFRINGE ANY OWNERSHIP RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR
 FITNESS FOR A PARTICULAR PURPOSE.
